import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:epson_epos2/epson_epos2.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
//  String _platformVersion = 'Unknown';
  List<Map<String, String>> _printers;

  String _errorReason;

  @override
  void initState() {
    super.initState();
    initPlatformState();
    _printers = <Map<String, String>>[];
    _errorReason = '';
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    List<Map<String, String>> printers = <Map<String, String>>[];
    String errorReason = '';
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
//      platformVersion = await EpsonEpos2.platformVersion;
      printers = await EpsonEpos2.discoverPrinters;
    } on PlatformException {
//      platformVersion = 'Failed to get platform version.';
      errorReason = 'Error discovering printers';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
//      _platformVersion = platformVersion;
      _printers = printers;
      _errorReason = errorReason;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Text(
            _errorReason.isNotEmpty
                ? _errorReason
                : "Found: ${_printers.length.toString()} printers",
          ),
        ),
      ),
    );
  }
}
