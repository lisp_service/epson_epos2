import 'dart:async';

import 'package:flutter/services.dart';

class EpsonEpos2 {
  static const MethodChannel _channel =
      const MethodChannel('epson_epos2');

  static Future<List<Map<String, String>>> get discoverPrinters async {
    final List<Map<String, String>> printers =
    await _channel.invokeMethod('startDiscovery');
    return printers;
  }
}
