#import "EpsonEpos2Plugin.h"
#if __has_include(<epson_epos2/epson_epos2-Swift.h>)
#import <epson_epos2/epson_epos2-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "epson_epos2-Swift.h"
#endif

@implementation EpsonEpos2Plugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftEpsonEpos2Plugin registerWithRegistrar:registrar];
}
@end
