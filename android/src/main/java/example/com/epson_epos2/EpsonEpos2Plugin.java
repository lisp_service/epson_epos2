package example.com.epson_epos2;

import android.content.Context;
import android.util.Log;
import android.widget.SimpleAdapter;

import androidx.annotation.NonNull;

import com.epson.epos2.Epos2Exception;
import com.epson.epos2.discovery.DeviceInfo;
import com.epson.epos2.discovery.Discovery;
import com.epson.epos2.discovery.DiscoveryListener;
import com.epson.epos2.discovery.FilterOption;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;

import java.util.ArrayList;
import java.util.HashMap;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/** EpsonEpos2Plugin */
public class EpsonEpos2Plugin implements FlutterPlugin, MethodCallHandler {
  private static final String CHANNEL = "epson_epos2";

  private static EpsonEpos2Plugin instance;
  private static MethodChannel channel;
  //    public static Printer mPrinter = null;
  private static int mPrinterModel = Printer.TM_U220;
  private static int mPrinterLang = Printer.MODEL_ANK;
  private Printer mPrinter = null;
  private Context mContext = null;
  private Result pendingResult;
  private ArrayList<HashMap<String, String>> mPrinterList = null;
  private SimpleAdapter mPrinterListAdapter = null;
  private Registrar mRegistrar;

  private final Object initializationLock = new Object();

  static {
    try {
      System.loadLibrary("libepos2.so");
    } catch (UnsatisfiedLinkError e) {
      Log.e("UnsatisfiedLinkError", "-----------------------" + e.getMessage());
    } catch (Exception e) {
      Log.e("Exception", "------------------------" + e.getMessage());
    }
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    this.pendingResult = result;
    String arg = call.arguments();

    switch (call.method) {
      case "startDiscovery":
        startDiscovery(result);
        break;
      case "stopDiscovery":
        stopDiscovery(result);
        break;
      case "connectPrinter":
        connectPrinter(arg, result);
        break;
      case "disconnectPrinter":
        disconnectPrinter(result);
        break;
      default:
        result.notImplemented();
        break;
    }
  }

  private void startDiscovery(Result result) {
    FilterOption mFilterOption = new FilterOption();
    mFilterOption.setDeviceType(Discovery.TYPE_PRINTER);
    mFilterOption.setEpsonFilter(Discovery.FILTER_NAME);
    try {
      Discovery.start(this.mContext, mFilterOption, discoveryListener);
      result.success(mPrinterList);
    } catch (Epos2Exception e) {
      Log.e(CHANNEL, "Error discovering printer: " + e.getErrorStatus(), e);
      result.error(e.getMessage(), null, e.getCause());
    }
  }

  private void stopDiscovery(Result result) {
    while (true) {
      try {
        Discovery.stop();
        result.success("Discovery stopped");
        break;
      } catch (Epos2Exception e) {
        Log.e(CHANNEL, "Error stopping printer: " + e.getErrorStatus(), e);
        result.error(e.getMessage(), null, e.getCause());
      }
    }
  }

  private ReceiveListener receiveListener = new ReceiveListener() {
    @Override
    public void onPtrReceive(final Printer printer, final int code, final PrinterStatusInfo status, final String printJobId) {
      Log.d(CHANNEL, "print success. status: " + status.getErrorStatus());

      new Thread(new Runnable() {
        @Override
        public void run() {
          disconnectPrinter(pendingResult);
        }
      }).start();
    }
  };

  private void connectPrinter(final String printerURL, final Result result) {
    Log.d(CHANNEL, "Connecting Printer");

    try {
      mPrinter = new Printer(mPrinterModel, mPrinterLang, this.mContext);
      mPrinter.setReceiveEventListener(receiveListener);
    } catch (Epos2Exception e) {
      result.error("Error creating printer: ", e.getMessage(), null);
      Log.e(CHANNEL, "Error creating printer: " + e.getErrorStatus(), e);
      return;
    }

    try {
      mPrinter.connect("TCP:" + printerURL, Printer.PARAM_DEFAULT);
    } catch (Epos2Exception e) {
      result.error("Error connecting printer: ", e.getMessage(), null);
      Log.e(CHANNEL, "Error connecting printer: " + e.getErrorStatus(), e);
      return;
    }

    try {
      mPrinter.beginTransaction();
    } catch (Epos2Exception e) {
      result.error("Error beginning transaction", e.getMessage(), null);
      Log.e(CHANNEL, "Error beginning transaction", e);
      return;
    }

    result.success("CONNECTION_OK");
  }

  private void disconnectPrinter(Result result) {
    if (mPrinter == null) {
      return;
    }

    try {
      mPrinter.endTransaction();
    } catch (Epos2Exception e) {
      result.error("Error ending transaction: ", e.getMessage(), null);
      Log.e(CHANNEL, "Error ending transaction: " + e.getErrorStatus(), e);
      return;
    }

    try {
      mPrinter.disconnect();
    } catch (Epos2Exception e) {
      result.error("Error disconnecting printer: ", e.getMessage(), null);
      Log.e(CHANNEL, "Error disconnecting printer: " + e.getErrorStatus(), e);
      return;
    }

    mPrinter.clearCommandBuffer();
    mPrinter.setReceiveEventListener(null);
    mPrinter = null;

    result.success("DISCONNECT_OK");
  }


  private boolean isPrintable(PrinterStatusInfo status) {
    if (status == null) {
      return false;
    }

    if (status.getConnection() == Printer.FALSE) {
      return false;
    } else if (status.getOnline() == Printer.FALSE) {
      return false;
    }

    return true;
  }

  private void onAttachedToEngine(BinaryMessenger messenger) {
    synchronized (initializationLock) {
      if (channel != null) {
        return;
      }
      channel = new MethodChannel(messenger, CHANNEL);
      channel.setMethodCallHandler(this);
    }
  }

  private DiscoveryListener discoveryListener = new DiscoveryListener() {
    @Override
    public void onDiscovery(final DeviceInfo deviceInfo) {
      mRegistrar.activity().runOnUiThread(new Runnable() {
        @Override
        public synchronized void run() {
          HashMap<String, String> item = new HashMap<String, String>();
          item.put("PrinterName", deviceInfo.getDeviceName());
          item.put("Target", deviceInfo.getTarget());
          mPrinterList.add(item);
          mPrinterListAdapter.notifyDataSetChanged();
        }
      });
    }
  };

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding binding) {
//        onAttachedToEngine(binding.getBinaryMessenger());
    final MethodChannel channel = new MethodChannel(binding.getFlutterEngine().getDartExecutor(), CHANNEL);
    channel.setMethodCallHandler(this);
    mContext = binding.getApplicationContext();
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
    channel = null;
  }

//  @Override
//  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
//    final MethodChannel channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "epson_epos2");
//    channel.setMethodCallHandler(new EpsonEpos2Plugin());
//  }

  // This static function is optional and equivalent to onAttachedToEngine. It supports the old
  // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
  // plugin registration via this function while apps migrate to use the new Android APIs
  // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
  //
  // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
  // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
  // depending on the user's project. onAttachedToEngine or registerWith must both be defined
  // in the same class.
//  public static void registerWith(Registrar registrar) {
//    final MethodChannel channel = new MethodChannel(registrar.messenger(), "epson_epos2");
//    channel.setMethodCallHandler(new EpsonEpos2Plugin());
//  }
//
//  @Override
//  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
//    if (call.method.equals("getPlatformVersion")) {
//      result.success("Android " + android.os.Build.VERSION.RELEASE);
//    } else {
//      result.notImplemented();
//    }
//  }
//
//  @Override
//  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
//  }
}
